import React from 'react';
import {shallow} from "enzyme";
import {AddNewToDoTaskComponent} from "./AddNewToDoTask";

describe('AddToDoNewTask', function () {
    describe('addNewTask', function () {
        it('should verify if the Add New Task button have clicked', function () {
            const props = {
                addNewTask : jest.fn(),
            }
            const AddToDoNewTaskComp = shallow(
                <AddNewToDoTaskComponent {...props}/>
            );
            
            AddToDoNewTaskComp.find('button').simulate('click');
            expect(props.addNewTask).toHaveBeenCalled();
        });

        it('should verify if the text is changed on onChange of the input field', function () {
            const props = {
                changeNewTaskText : jest.fn(),
            }
            const AddToDoNewTaskComp = shallow(
                <AddNewToDoTaskComponent {...props}/>
            );
            AddToDoNewTaskComp.find('input').simulate('change', { target: { value: 'test' } });
            expect(props.changeNewTaskText).toHaveBeenCalled();
        });
    });
});