import React from 'react';
import ListItem from './ListItem'
import {shallow} from "enzyme";

describe('ListItem', function () {
    it('should ', function () {
        const ListItemTag = shallow(
            <ListItem
                done={false}
                text="List Item"
            />
        );

        const inputTag = ListItemTag.find('input');
        const spanTag = ListItemTag.find('span');

        expect(inputTag.prop('checked')).toBe(false);
        expect(spanTag.text()).toBe('List Item');
    });
});