import React from 'react';
import ListItem from "./ListItem";
import {shallow} from "enzyme";
import {ToDoListComponent} from "./ToDoList";

describe('ToDoList', function () {
    it('should verify ', function () {
        const ToDoListTag = shallow(
            <ToDoListComponent
                toDoList={[
                    {done: true, text: 'List Item'},
                    {done: false, text: 'List Item Two'}
                ]}
            >
            </ToDoListComponent>
        );

        const listItemNodes = ToDoListTag.find(ListItem);
        const itemNameOneText = listItemNodes.at(0).prop('text');
        const itemNameOneDone = listItemNodes.at(0).prop('done');

        const itemNameTwoText = listItemNodes.at(1).prop('text');
        const itemNameTwoDone = listItemNodes.at(1).prop('done');

        expect(itemNameOneText).toBe('List Item');
        expect(itemNameOneText.length).toBe(9);
        expect(itemNameOneDone).toBeTruthy();

        expect(itemNameTwoText).toBe('List Item Two');
        expect(itemNameTwoText.length).toBe(13);
        expect(itemNameTwoDone).toBeFalsy()
    });
});