const CHANGE_NEW_TASK_NAME = 'CHANGE_NEW_TASK_NAME';
const ADD_NEW_TASK = 'ADD_NEW_TASK';
const TOGGLE_TASK_COMPLETION = 'TOGGLE_TASK_COMPLETION';

const initialState = {
    toDoList: [
        {done: true, text: 'Javasrcipt'},
        {done: false, text: 'Ruby on Rails'},
        {done: true, text: 'React'},
        {done: true, text: 'Java'},
        {done: true, text: 'Redux'},
        {done: false, text: 'Spring'},
    ],
    newTask: {
        text: '',
    }
};

export const changeNewTaskText = (newTaskName) => ({
    type: CHANGE_NEW_TASK_NAME,
    data: {
        newTaskName,
    }
});

export const addNewTask = () => ({
    type: ADD_NEW_TASK,
});

export const toggleTaskCompletion = (index) => ({
    type: TOGGLE_TASK_COMPLETION,
    data: {
        index
    }
})

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_NEW_TASK_NAME: {
            // const newState = {...state};
            // newState.newTask.text = action.data.newTaskName
            // return newState;

            return {...state, newTask: {text: action.data.newTaskName}};
        }

        case ADD_NEW_TASK: {
            let newTask = {
                done: false,
                text: state.newTask.text,
            };

            return {...state, newTask: {text: ''}, toDoList: [...state.toDoList, newTask]};
        }

        case TOGGLE_TASK_COMPLETION: {
            return {
                ...state,
                toDoList: state.toDoList.map((item, index) => {
                    if (index === action.data.index) {
                        return {...item, done: !item.done}
                    }
                    return item;
                })
            }
        }

        default :
            return state;
    }
};

export default reducer;