import reducer from './Reducer';

import {addNewTask, changeNewTaskText, toggleTaskCompletion} from "./Reducer";

describe('Reducer', function () {
    describe('action addNewTask', function () {
        it('should return initial todoList if no action is fired', function () {
            let initialState = {
                toDoList: [
                    {done: true, text: 'JavaSrcipt'},
                    {done: false, text: 'Ruby on Rails'},
                    {done: true, text: 'React'},
                    {done: true, text: 'Java'},
                    {done: true, text: 'Redux'},
                    {done: false, text: 'Spring'},

                ],
                newTask: {
                    text: '',
                }
            }

            const receivedState = reducer(initialState, {type: 'DEFAULT_ACTION'});

            expect(receivedState).toEqual(initialState);

        });

        it('should add a new task when it is fired', function () {
            let initialState = {
                toDoList: [
                    {done: true, text: 'JavaSrcipt'},
                    {done: false, text: 'Ruby on Rails'},
                    {done: true, text: 'React'},
                    {done: true, text: 'Java'},
                    {done: true, text: 'Redux'},
                    {done: false, text: 'Spring'},

                ],
                newTask: {
                    text: '',
                }
            }

            let tempState = reducer(initialState, changeNewTaskText('Python'));
            const receivedState = reducer(tempState, addNewTask());

            const expectedState = {
                toDoList: [
                    {done: true, text: 'JavaSrcipt'},
                    {done: false, text: 'Ruby on Rails'},
                    {done: true, text: 'React'},
                    {done: true, text: 'Java'},
                    {done: true, text: 'Redux'},
                    {done: false, text: 'Spring'},
                    {done: false, text: 'Python'},
                ],
                newTask: {
                    text: '',
                }
            }
            expect(receivedState).toEqual(expectedState);

        });
    });

    describe('action toggleTaskCompletion', function () {
        it('should toggle task completion attribute of the given task', function () {
            let initialState = {
                toDoList: [
                    {done: true, text: 'JavaSrcipt'},
                    {done: false, text: 'Ruby on Rails'},
                ],
                newTask: {
                    text: '',
                }
            }

            const receivedState = reducer(initialState, toggleTaskCompletion(0));

            expect(initialState.toDoList).not.toBe(receivedState.toDoList);
            expect(initialState.toDoList[0]).not.toBe(receivedState.toDoList[0]);
            expect(receivedState).toEqual({
                toDoList: [
                    {done: false, text: 'JavaSrcipt'},
                    {done: false, text: 'Ruby on Rails'},
                ],
                newTask: {
                    text: '',
                }
            })

        });
    });
});