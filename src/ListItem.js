import React from 'react';

const ListItem = (props) =>
    <li>
        <input
            type="checkbox"
            checked={props.done}
            onChange={props.toggleTaskCompletion}
        /><span>{props.text}</span>
    </li>

export default ListItem;