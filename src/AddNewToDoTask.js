import {addNewTask, changeNewTaskText} from "./Reducer";
import React from "react";
import {connect} from "react-redux";
import Button from '@material-ui/core/Button';

export const AddNewToDoTaskComponent = (props) =>
    <div>
        <label>Task: </label>
        <input type="text"
               value={props.newTaskName}
               placeholder='Enter Task...'
               onChange={(event) => props.changeNewTaskText(event.target.value)}
        />
        <Button  variant="contained" color="primary" onClick={() => props.addNewTask()}>
            Add Task
        </Button>
    </div>

const mapStateAsPropsForAddNewTask = (state) => ({
    newTaskName: state.newTask.text,
});

const mapDispatcherAsPropsForAddNewTask = (dispatch) => ({
    addNewTask: () => dispatch(addNewTask()),
    changeNewTaskText: (text) => dispatch(changeNewTaskText(text))
});


const AddNewToDoTask = connect(mapStateAsPropsForAddNewTask, mapDispatcherAsPropsForAddNewTask)(AddNewToDoTaskComponent);

export default AddNewToDoTask;