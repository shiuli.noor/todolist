import {toggleTaskCompletion} from "./Reducer";
import React from "react";
import ListItem from './ListItem';
import {connect} from "react-redux";

export const ToDoListComponent = (props) =>
    <ul>
        {
            props.toDoList
                .map((product, index) =>
                    <ListItem
                        done={props.toDoList[index].done}
                        text={props.toDoList[index].text}
                        key={index}
                        toggleTaskCompletion = {() => props.toggleTaskCompletion(index)}
                    />
                )
        }
    </ul>;

const mapStateAsPropsForToDoList = (state) => ({
    toDoList: state.toDoList,
});

const mapDispatchAsPropsForToDoList = (dispatch) => ({
    toggleTaskCompletion: (index) => dispatch(toggleTaskCompletion(index))
});

const ToDoList = connect(mapStateAsPropsForToDoList, mapDispatchAsPropsForToDoList)(ToDoListComponent);

export default ToDoList;