import React from 'react';
import AddNewToDoTask from './AddNewToDoTask';
import ToDoList from './ToDoList';

const App = () =>
    <div>
        <AddNewToDoTask/>
        <ToDoList/>
    </div>

export default App;
